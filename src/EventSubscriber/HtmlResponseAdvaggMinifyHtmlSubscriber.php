<?php



namespace Drupal\advagg_minify_html\EventSubscriber;

use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\advagg_minify_html\Minifier\HtmlMin;

/**
 * Class HtmlResponseAdvaggMinifyHtmlSubscriber.
 *
 * @package Drupal\advagg_minify_html
 */
class HtmlResponseAdvaggMinifyHtmlSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    // Run as the last possible subscriber.
    $events[KernelEvents::RESPONSE][] = ['onRespond', -10001];

    return $events;
  }

  /**
   * Transforms a HtmlResponse to a BigPipeResponse.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to process.
   */
  public function onRespond(FilterResponseEvent $event) {

    $response = $event->getResponse();

    if (!$response instanceof HtmlResponse) {

      return;

    }

    $minify_content = HtmlMin::minify($response->getContent(), FALSE, FALSE);

    $response->setContent($minify_content);

    $event->setResponse($response);

  }

}
